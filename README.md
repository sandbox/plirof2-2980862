# d7_oauth2_gsis module
Module for Drupal 7 that allows connecting to an Oauth2 server. This is made for the Greek GSIS server but it could be used for other servers with modifications.

## Note:
Ath this moment the production site do not provide custom logout redirect (?url=.. parameter). This parameter works in the test site

## Usage
After enabling oauth2_gsis module ,goto Structure/Block and put the Oauth2_Gsis login block to the Header (or anywhere you prefer).
You will see a button on your WebSite that will redirect you to the Oauth2 server.

## Modifications/configuration
Modifications/configuration is done by modifying these oauth2_gsis.module file functions:
You must put the correct redirect URLs

function oauth2_getUrl($url_name){

function oauth2_getConData($code){

## Demo server
Before connectiong to the official/pilot GSIS servers you can try connecting to my NodeJS server modification here : https://github.com/plirof2/fake-oauth2-server
It emulates GSIS server and you can have it return whatever data you prefer


